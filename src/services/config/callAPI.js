import axios from "axios";
export const callAPI = (method, url, data) => {
  return axios({
    baseURL: 'http://tickets-tahiti-api.bustedgame.site/api/v1',
    timeout: 20000,
    headers: {
      'content-type': 'application/json',
      Authorization: localStorage.getItem('auth') ? JSON.parse(localStorage.getItem('auth')).token : ''
    },
    method,
    url,
    data,
  })
}