import { callAPI } from './callAPI'

export const handleError = (error) => {

}

const makeRequest = async (
  method,
  url,
  data) => {
  try {
    return callAPI(method, url, data);
  } catch (error) {
    handleError(error);
  }
}

export default makeRequest