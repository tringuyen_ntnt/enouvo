import request from '../config/makeRequest'


const getMovies = (limit, offset) => {
    return new Promise((rs, rj) => {
        request('get', `/movies?limit=${limit}&offset=${offset}&filter=%7B%7D`)
            .then(res => rs(res.data))
            .catch(err => rj(err))
    })
}

export {
    getMovies
}