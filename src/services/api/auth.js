import request from '../config/makeRequest'


const login = (data) => {
    return new Promise((rs, rj) => {
        request('post', `/auth/login`, data)
            .then(res => rs(res.data))
            .catch(err => rj(err.response))
    })
}

const register = (data) => {
    return new Promise((rs, rj) => {
        request('post', '/auth/register', data)
            .then(res => rs(res.data))
            .catch(err => rj(err.response))
    })
}

export {
    login,
    register
}