import React, { useContext, useEffect, useState } from "react";
import { Button, Card, CardBody, CardText, CardTitle, Col, Container, Row } from "reactstrap";
import BottomScrollListener from 'react-bottom-scroll-listener';

import { AuthContext } from "../context/auth";
import { AppContext } from "../context/AppContext";
import { getMovies } from '../services/api/movies'
import '../assets/css/movies.css'

function Movies(props) {
  const { logout } = useContext(AuthContext);
  const { appState, setAppStateByKey } = useContext(AppContext);
  const [offset, setOffset] = useState(0);
  const [total, setTotal] = useState(0);
  let limit = 10;

  useEffect(() => {
    getMovies(limit, offset)
      .then(data => {
        setTotal(data.total);
        setAppStateByKey('movies', data.results);
        setOffset(limit);
      })
      .catch(err => console.log(err))
  }, [])

  const logOut = () => {
    logout();
  }

  const handleContainerOnBottom = () => {
    if (appState.movies.length >= total) return
    getMovies(limit, offset).then(data => {
      setAppStateByKey('movies', [
        ...appState.movies,
        ...data.results]
      )
      setOffset(offset + limit)
    });
  }



  return (
    <BottomScrollListener onBottom={handleContainerOnBottom}>
      <Container>
        <Row>
          <Col md="2">
            <Button onClick={logOut}>Log out</Button>
          </Col>
          <Col md="10">
            <Row>
              {appState.movies.map(movie => {
                return (
                  <Col md='4' key={movie.id}>
                    <Card>
                      <div
                        className="cointainer-img"
                        style={{
                          backgroundImage: "url(" + movie.thumbnail.src + ")",
                        }}
                      >
                      </div>
                      <CardBody>
                        <CardTitle>{movie.name}</CardTitle>
                        <CardText>{movie.description.substring(0, 70)}...</CardText>
                        {/* <Button>Button</Button> */}
                      </CardBody>
                    </Card>
                  </Col>
                )
              })}
            </Row>
          </Col>
        </Row>
      </Container>
    </BottomScrollListener>
  );
}

export default Movies;