import React, { useState, useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';

import { AuthContext } from "../context/auth";
import { login } from '../services/api/auth';
import { Toast } from '../components/Notification';


function Login(props) {

  const [formData, setFormData] = useState({
    "email": "",
    "password": "",
  });

  const { setAuthTokens, auth } = useContext(AuthContext);

  const history = useHistory();

  if (auth) {
    history.push('/')
  }

  const handleChange = (event) => {
    setFormData({
      ...formData,
      [`${event.target.name}`]: event.target.value
    })
  }


  const handleSubmit = (e) => {
    e.preventDefault();
    login(formData)
      .then(data => {
        setAuthTokens(
          data
        );
        history.push('/')
      }).catch(e => {
        Toast(e.data.message, 'error');
      });

  }

  return (
    <Container>
      <h1>Login</h1>
      <Form >
        <FormGroup>
          <Label for="email">Email</Label>
          <Input type="email" name="email" id="email" required={true} placeholder="email" onChange={handleChange} />
        </FormGroup>
        <FormGroup>
          <Label for="password">Password</Label>
          <Input type="password" name="password" id="password" required placeholder="password" onChange={handleChange} />
        </FormGroup>
        <Button type="button" onClick={handleSubmit}>Submit</Button>
      </Form>
      <Link to="/signup">Don't have an account?</Link>
    </Container>
  );
}

export default Login;