import React, { useState, useContext } from "react";
import { Link, useHistory } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';

import { AuthContext } from "../context/auth";
import { register } from '../services/api/auth';
import { Toast } from '../components/Notification';


function Signup() {

  const [formData, setFormData] = useState({
    "email": "",
    "password": "",
    "firstName": "",
    "lastName": "",
    "phoneNumber": "",
    "address": "",
    "additionalAddress": [],
    "city": "",
    "postCode": "",
    "country": ""
  });

  const { setAuthTokens, auth } = useContext(AuthContext);

  const history = useHistory();

  if (auth) {
    history.push('/')
  }

  const handleChange = (event) => {
    if (event.target.name === "additionalAddress") {
      setFormData({
        ...formData,
        [`${event.target.name}`]: [event.target.value]
      })
    } else
      setFormData({
        ...formData,
        [`${event.target.name}`]: event.target.value
      })
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    register(formData)
      .then(data => {
        setAuthTokens(
          data
        );
        history.push('/');
      }).catch(e => {
        Toast(e.data.message, 'error');
      });
  }



  return (
    <Container>
      <h1>Register</h1>
      <Form >
        <Row form>
          <Col md={6}>
            <FormGroup>
              <Label for="email">Email</Label>
              <Input type="email" name="email" id="email" placeholder="email" onChange={handleChange} />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="password">Password</Label>
              <Input type="password" name="password" id="password" placeholder="password" onChange={handleChange} />
            </FormGroup>
          </Col>
        </Row>

        <Row form>
          <Col md={4}>
            <FormGroup>
              <Label for="firstName">First Name</Label>
              <Input type="text" name="firstName" id="firstName" onChange={handleChange} />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="lastName">Last Name</Label>
              <Input type="text" name="lastName" id="lastName" onChange={handleChange} />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="phoneNumber">Phone Number</Label>
              <Input type="text" name="phoneNumber" id="phoneNumber" onChange={handleChange} />
            </FormGroup>
          </Col>
        </Row>

        <FormGroup>
          <Label for="address">Address</Label>
          <Input type="text" name="address" id="address" placeholder="1234 Main St" onChange={handleChange} />
        </FormGroup>
        <FormGroup>
          <Label for="additionalAddress">Additional Address</Label>
          <Input type="text" name="additionalAddress" id="additionalAddress" placeholder="Apartment, studio, or floor" onChange={handleChange} />
        </FormGroup>


        <Row form>
          <Col md={4}>
            <FormGroup>
              <Label for="city">City</Label>
              <Input type="text" name="city" id="city" onChange={handleChange} />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="postCode">Post Code</Label>
              <Input type="text" name="postCode" id="postCode" onChange={handleChange} />
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              <Label for="country">Country</Label>
              <Input type="text" name="country" id="country" onChange={handleChange} />
            </FormGroup>
          </Col>
        </Row>

        <FormGroup check>
          <Input type="checkbox" name="check" id="exampleCheck" onChange={handleChange} />
          <Label for="exampleCheck" check>Check me out</Label>
        </FormGroup>
        <Button type="button" onClick={handleSubmit}>Submit</Button>
      </Form>
      <Link to="/login">Already have an account?</Link>
    </Container>
  )
}

export default Signup;

