import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import PrivateRoute from './PrivateRoute';
import Movies from "./pages/Movies";
import AuthContextProvider from "./context/auth";
import AppContextProvider from "./context/AppContext";
import Login from "./pages/Login";
import Signup from './pages/Signup';
import { ToastContainer } from 'react-toastify';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';

function App(props) {

  return (
    <AuthContextProvider>
      <AppContextProvider>
        <ToastContainer />
        <Router>
          <Route path="/login" component={Login} />
          <Route path="/signup" component={Signup} />
          <PrivateRoute exact path="/" component={Movies} />
        </Router>
      </AppContextProvider>
    </AuthContextProvider>
  );
}

export default App;