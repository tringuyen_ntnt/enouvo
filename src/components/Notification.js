import { toast } from 'react-toastify';

const Toast = (msg, type="info") => {
    toast[type](msg, {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    });
}
const alert = (msg) => {

}

export {
    Toast,
    alert
}