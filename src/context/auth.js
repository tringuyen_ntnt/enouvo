
import React, { createContext, useState, useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';


export const AuthContext = createContext();

const AuthContextProvider = ({ children }) => {

  const [auth, setAuth] = useState();
  useEffect(()=>{
    if(localStorage.getItem('auth')){
      setAuth(localStorage.getItem('auth'))
    }
  },[])
  const setAuthTokens = useCallback((value, key) => {
    if (value !== undefined) {
      setAuth(value)
      localStorage.setItem('auth', JSON.stringify(value))
    }
  }, []);

  const logout = useCallback((value, key) => {
    setAuth(null);
    localStorage.removeItem('auth')
  }, []);

  return (
    <AuthContext.Provider
      value={{
        setAuthTokens,
        logout,
        auth,
        setAuth
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

AuthContextProvider.propTypes = {
  children: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default AuthContextProvider;