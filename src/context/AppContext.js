import React, { createContext, useState, useCallback } from 'react';
import PropTypes from 'prop-types';

const defaultAppState = {
  movies: [],
};

export const AppContext = createContext({
  appState: defaultAppState,
});

const AppContextProvider = ({ children }) => {
  const langList = [
    {
      id: 1,
      langName: 'English',
      langCode: 'en',
      countryCode: 'us',
    },
    {
      id: 2,
      langName: '中文 (繁)',
      langCode: 'zh-HK',
      countryCode: 'hk',
    },
    {
      id: 3,
      langName: '中文 (简体)',
      langCode: 'zh-Hans',
      countryCode: 'hk',
    },
  ];

  const [currentLang, setCurrentLang] = useState(() => {
    // use callback if the initial state is got from a function
    // => prevent multiple render of that function
    const currentLangCode = window.localStorage.getItem('ljs-lang') || 'en';
    const initialLang = langList.find((l) => l.langCode === currentLangCode);
    return initialLang;
  });

  const [appState, setAppState] = useState(defaultAppState);

  const setAppStateByKey = useCallback((key, value) => {
    if (value !== undefined) {
      setAppState((oldState) => ({
        ...oldState,
        [key]: value,
      }));
    }
  }, []);

  return (
    <AppContext.Provider
      value={{
        currentLang,
        setCurrentLang,
        langList,
        appState,
        setAppStateByKey,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

AppContextProvider.propTypes = {
  children: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default AppContextProvider;